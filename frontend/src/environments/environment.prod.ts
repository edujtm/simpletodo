export const environment = {
  production: true,
  backendApi: 'https://backend-simple-todo.herokuapp.com'
};
