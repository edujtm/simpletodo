FROM mcr.microsoft.com/dotnet/sdk:5.0-alpine

RUN dotnet tool install -g dotnet-reportgenerator-globaltool

ENV PATH ${PATH}:/root/.dotnet/tools
