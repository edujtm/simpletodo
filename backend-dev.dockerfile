FROM mcr.microsoft.com/dotnet/sdk:5.0 as build-env

WORKDIR /build

COPY backend/SimpleTodo/SimpleTodo.csproj .
RUN dotnet restore
RUN dotnet tool install --global dotnet-ef --version 5.0.9
ENV PATH $PATH:/root/.dotnet/tools
COPY backend/SimpleTodo/ .
RUN dotnet ef database update
RUN dotnet publish -c Release --output ./dist

FROM mcr.microsoft.com/dotnet/aspnet:5.0 as runtime
WORKDIR /app
COPY --from=build-env /build/db.sqlite .
COPY --from=build-env /build/dist .
ENV ASPNETCORE_URLS "http://0.0.0.0:5000/"
ENTRYPOINT ["dotnet", "SimpleTodo.dll"]

