using System;
using System.Linq;
using System.Data.Common;
using Microsoft.Data.Sqlite;
using Microsoft.EntityFrameworkCore;

using SimpleTodo.Infra.Persistence;

namespace SimpleTodo.UnitTests.Fixtures
{
    public class MockContextFixture : IDisposable
    {

        private DbConnection _connection;
        public EFContext Context { get; private set; }

        public MockContextFixture()
        {

            _connection = CreateSqliteConnection();
            var options = new DbContextOptionsBuilder<EFContext>()
                            .UseSqlite(_connection)
                            .Options;
            Context = new EFContext(options);
            Context.ChangeTracker.QueryTrackingBehavior = QueryTrackingBehavior.NoTracking;
            Context.Database.EnsureCreated();
        }

        private DbConnection CreateSqliteConnection()
        {
            var connection = new SqliteConnection("Filename=:memory:");

            connection.Open();

            return connection;
        }

        public void DetachAllEntities()
        {
            var changedEntriesCopy = Context.ChangeTracker.Entries()
                .Where(e => e.State == EntityState.Added ||
                            e.State == EntityState.Modified ||
                            e.State == EntityState.Deleted || 
                            e.State == EntityState.Unchanged)
                .ToList();

            foreach (var entry in changedEntriesCopy)
                entry.State = EntityState.Detached;
        }

        public void Dispose() 
        {
            Context.Database.EnsureDeleted();
            Context.Dispose();
            _connection.Dispose();
        }
    }
}
