using System;
using System.Threading;
using System.Threading.Tasks;

using AutoMapper;
using Xunit;
using FluentAssertions;

using SimpleTodo.Application.Todos.Commands.CreateTodo;
using SimpleTodo.Application.Todos.DataTransfer;
using SimpleTodo.UnitTests.Fixtures;

namespace SimpleTodo.UnitTests.Tests.Todos
{
    public class CreateTodoTests : IDisposable
    {

        private readonly MockContextFixture _fixture;
        private readonly MapperConfiguration _mapperCfg = new MapperConfiguration(cfg =>
        {
            cfg.AddProfile(new TodosMapperProfile());
        });

        public CreateTodoTests()
        {
            _fixture = new MockContextFixture();
        }

        public void Dispose()
        {
            _fixture.Dispose();
        }

        [Fact]
        public async Task CreateTodoCommandHandler_ShouldReturnCreatedTodo()
        {
            var handler = new CreateTodoCommandHandler(_fixture.Context, _mapperCfg.CreateMapper());

            var command = new CreateTodoCommand
            {
                Name = "Some Task",
                Description = "I need to do some task!",
                Status = false
            };

            var response = await handler.Handle(command, CancellationToken.None);

            response.Todo.Should().BeEquivalentTo(command);
        }

        [Fact]
        public async Task CreateTodoCommandHandler_ShouldReturnCurrentTodosList()
        {
            var handler = new CreateTodoCommandHandler(_fixture.Context, _mapperCfg.CreateMapper());

            var command = new CreateTodoCommand
            {
                Name = "Another Task",
                Description = "I've made another task",
                Status = true
            };

            var response = await handler.Handle(command, CancellationToken.None);

            response.Todos.Should().SatisfyRespectively(
                first =>
                {
                    first.Should().BeEquivalentTo(command);
                });
        }

        [Fact]
        public async Task CreateTodoCommandHandler_ShouldReturnConfirmationMessage()
        {
            var handler = new CreateTodoCommandHandler(_fixture.Context, _mapperCfg.CreateMapper());

            var command = new CreateTodoCommand
            {
                Name = "Another Task",
                Description = "I've made another task",
                Status = true
            };

            var response = await handler.Handle(command, CancellationToken.None);

            response.Message.Should().NotBeNull().And.NotBeEmpty();
        }

        [Fact]
        public async Task CreateTodoCommandHandler_ShouldPersistCreatedTodo()
        {
            var handler = new CreateTodoCommandHandler(_fixture.Context, _mapperCfg.CreateMapper());

            var command = new CreateTodoCommand
            {
                Name = "Another Task",
                Description = "I've made another task",
                Status = true
            };

            var response = await handler.Handle(command, CancellationToken.None);

            _fixture.Context.Todos.Should().HaveCount(1);
        }
    }
}
