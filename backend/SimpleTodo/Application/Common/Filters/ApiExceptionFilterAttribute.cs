using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;

using SimpleTodo.Application.Common.Exceptions;

namespace SimpleTodo.Application.Common.Filters
{
    public class ApiExceptionFilterAttribute : ExceptionFilterAttribute
    {
        public readonly IDictionary<Type, Action<ExceptionContext>>  _exceptionHandlers;

        public ApiExceptionFilterAttribute()
        {
            _exceptionHandlers = new Dictionary<Type, Action<ExceptionContext>>
            {
                { typeof(InvalidRequestException), HandleInvalidRequestException },
                { typeof(NotFoundException<>), HandleNotFoundException },
            };
        }

        public override void OnException(ExceptionContext context)
        {
            HandleException(context);

            base.OnException(context);
        }

        public void HandleException(ExceptionContext context)
        {
            Type type = context.Exception.GetType(); 
            if (_exceptionHandlers.ContainsKey(type))
            {
                _exceptionHandlers[type].Invoke(context);
                return;
            }

            if (type.IsGenericType && _exceptionHandlers.ContainsKey(type.GetGenericTypeDefinition()))
            {
                _exceptionHandlers[type.GetGenericTypeDefinition()].Invoke(context);
                return;
            }

            if (!context.ModelState.IsValid)
            {
                HandleInvalidModelStateException(context);
                return;
            }

            HandleUnkownException(context);
        }

        private void HandleInvalidRequestException(ExceptionContext context)
        {
            var exception = context.Exception as InvalidRequestException;

            var details = new {
                Title = "Request is not valid.",
                Errors = exception.Errors
            };

            context.Result = new BadRequestObjectResult(details);
            context.ExceptionHandled = true;
        }

        public void HandleInvalidModelStateException(ExceptionContext context)
        {
            var details = new {
                Title = "Request is not valid.",
                Errors = context.ModelState
            };

            context.Result = new BadRequestObjectResult(details);
            context.ExceptionHandled = true;
        }

        public void HandleNotFoundException(ExceptionContext context)
        {
            var details = new {
                Title = "Not Found",
                Errors = new List<string> { context.Exception.Message }
            };

            context.Result = new NotFoundObjectResult(details);
            context.ExceptionHandled = true;
        }

        public void HandleUnkownException(ExceptionContext context)
        {

            var details = new {
                Title = "Something unexpected occurred. Try again later.",
            };

            context.Result = new ObjectResult(details)
            {
                StatusCode = StatusCodes.Status500InternalServerError
            };
            context.ExceptionHandled = true;
        }
    }
}
