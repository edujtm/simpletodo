using System;
using System.Linq;
using System.Collections.Generic;

using FluentValidation.Results;

namespace SimpleTodo.Application.Common.Exceptions
{
    public class InvalidRequestException : Exception
    {
        public IDictionary<string, string[]> Errors { get; }

        public InvalidRequestException()
            : base("One or more validation failures have occurred.")
        {
            Errors = new Dictionary<string, string[]>();
        }

        public InvalidRequestException(IEnumerable<ValidationFailure> failures)
            : this()
        {
            Errors = failures
                .GroupBy(e => e.PropertyName, e => e.ErrorMessage)
                .ToDictionary(failureGroup => failureGroup.Key, failureGroup => failureGroup.ToArray());
        }
    }
}
