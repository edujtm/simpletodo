using System;


namespace SimpleTodo.Application.Common.Exceptions
{
    public class NotFoundException<T> : Exception 
    {
        public NotFoundException(string message)
            : base(message)
        {
        }

        public NotFoundException(string message, Exception innerException)
            : base(message, innerException)
        {
        }

        public NotFoundException(object key)
            : base($"Entity {typeof(T).Name} ({key}) was not found.") 
        {
        }
    }
}
