using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Collections.Generic;

using MediatR;
using FluentValidation;

using SimpleTodo.Application.Common.Exceptions;

namespace SimpleTodo.Application.Common.Behaviors
{
    public class ValidationBehavior<TRequest, TResponse> : IPipelineBehavior<TRequest, TResponse>
    {
        private readonly IEnumerable<IValidator<TRequest>> _validators;

        public ValidationBehavior(IEnumerable<IValidator<TRequest>> validators)
        {
            _validators = validators;
        }

        public async Task<TResponse> Handle(
            TRequest request, 
            CancellationToken cancelToken,
            RequestHandlerDelegate<TResponse> next
        )
        {

            if (_validators.Any())
            {
                var context = new ValidationContext<TRequest>(request);

                var validationResults = await Task.WhenAll(
                    _validators.Select(v => v.ValidateAsync(context, cancelToken))
                );

                var failures = validationResults.SelectMany(r => r.Errors)
                    .Where(f => f != null)
                    .ToList();

                if (failures.Count() != 0)
                {
                    throw new InvalidRequestException(failures);
                }
            }

            return await next();
        }
    }
}
