using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

using MediatR;
using AutoMapper;

using SimpleTodo.Domain.Entities;
using SimpleTodo.Infra.Persistence;
using SimpleTodo.Application.Todos.DataTransfer;
using SimpleTodo.Application.Common.Exceptions;

namespace SimpleTodo.Application.Todos.Commands.UpdateTodo
{
    public class UpdateTodoCommandHandler : IRequestHandler<UpdateTodoCommand, TodoActionViewModel>
    {
        private readonly EFContext _context;
        private IMapper _mapper;

        public UpdateTodoCommandHandler(
            EFContext context,
            IMapper mapper
        )
        {
            _context = context;
            _mapper = mapper;
        }

        public async Task<TodoActionViewModel> Handle(
            UpdateTodoCommand request,
            CancellationToken cancelToken
        )
        {
            var entity = await _context.Todos.FindAsync(request.Id);
            if (entity == null)
            {
                throw new NotFoundException<TodoItem>(request.Id);
            }

            entity.Name = request.Name ?? entity.Name;
            entity.Description = request.Description ?? entity.Description;
            entity.Status = request.Status ?? entity.Status;

            await _context.SaveChangesAsync(cancelToken);

            var todos = await _context.Todos.ToListAsync();

            return new TodoActionViewModel()
            {
                Message = "Todo Updated!",
                Todo = _mapper.Map<TodoItemViewModel>(entity),
                Todos = todos.Select(_mapper.Map<TodoItemViewModel>).ToList()
            };
        }
    }
}
