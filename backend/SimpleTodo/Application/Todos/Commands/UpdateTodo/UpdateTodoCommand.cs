
using MediatR;

using SimpleTodo.Application.Todos.DataTransfer;

namespace SimpleTodo.Application.Todos.Commands.UpdateTodo
{
    public class UpdateTodoCommand : IRequest<TodoActionViewModel>
    {
        public int Id { get; set; }

        public string Name  { get; set; }

        public string Description { get; set; }

        public bool? Status { get; set; }
    }
}
