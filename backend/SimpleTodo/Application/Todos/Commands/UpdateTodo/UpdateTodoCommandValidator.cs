
using FluentValidation;

namespace SimpleTodo.Application.Todos.Commands.UpdateTodo
{

    public class UpdateTodoCommandValidator : AbstractValidator<UpdateTodoCommand>
    {
        public UpdateTodoCommandValidator()
        {
            RuleFor(ut => ut.Name)
                .MinimumLength(1).WithMessage("name field must not be empty")
                .MaximumLength(100).WithMessage("Maximum characters length exceeded. ({MaxLength} máx.)");


            RuleFor(ut => ut.Description)
                .MinimumLength(1).WithMessage("description field must not be empty")
                .MaximumLength(300).WithMessage("Maximum characters length exceeded. ({MaxLength} máx.)");
        }
    }
}
