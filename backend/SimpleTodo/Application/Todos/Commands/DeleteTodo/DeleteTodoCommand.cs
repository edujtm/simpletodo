
using MediatR;

using SimpleTodo.Application.Todos.DataTransfer;

namespace SimpleTodo.Application.Todos.Commands.DeleteTodo
{
    public class DeleteTodoCommand : IRequest<TodoActionViewModel>
    {
        public int Id { get; set; }
    }
}
