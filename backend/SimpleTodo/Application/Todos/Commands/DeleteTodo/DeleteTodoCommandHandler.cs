using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

using MediatR;
using AutoMapper;

using SimpleTodo.Domain.Entities;
using SimpleTodo.Infra.Persistence;
using SimpleTodo.Application.Todos.DataTransfer;
using SimpleTodo.Application.Common.Exceptions;

namespace SimpleTodo.Application.Todos.Commands.DeleteTodo
{
    public class DeleteTodoCommandHandler : IRequestHandler<DeleteTodoCommand, TodoActionViewModel>
    {

        private readonly EFContext _context;
        private IMapper _mapper;

        public DeleteTodoCommandHandler(
            EFContext context,
            IMapper mapper
        )
        {
            _context = context;
            _mapper = mapper;
        }

        public async Task<TodoActionViewModel> Handle(DeleteTodoCommand request, CancellationToken cancelToken)
        {
            var entity = await _context.Todos.FindAsync(request.Id);

            if (entity == null)
            {
                throw new NotFoundException<TodoItem>(request.Id);
            }

            _context.Todos.Remove(entity);

            await _context.SaveChangesAsync(cancelToken);

            var todos = await _context.Todos.ToListAsync();

            return new TodoActionViewModel()
            {
                Message = "Todo Deleted!",
                Todo = _mapper.Map<TodoItemViewModel>(entity),
                Todos = todos.Select(_mapper.Map<TodoItemViewModel>).ToList()
            };
        }
    }
}
