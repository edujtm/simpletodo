
using MediatR;

using SimpleTodo.Application.Todos.DataTransfer;

namespace SimpleTodo.Application.Todos.Commands.CreateTodo
{
    public class CreateTodoCommand : IRequest<TodoActionViewModel>
    {
        public string Name { get; set; }

        public string Description { get; set; }

        public bool? Status { get; set; }
    }
}
