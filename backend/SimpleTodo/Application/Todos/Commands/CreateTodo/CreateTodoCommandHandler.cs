using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

using MediatR;
using AutoMapper;

using SimpleTodo.Domain.Entities;
using SimpleTodo.Infra.Persistence;
using SimpleTodo.Application.Todos.DataTransfer;

namespace SimpleTodo.Application.Todos.Commands.CreateTodo
{
    public class CreateTodoCommandHandler : IRequestHandler<CreateTodoCommand, TodoActionViewModel>
    {

        private readonly EFContext _context;
        private IMapper _mapper;

        public CreateTodoCommandHandler(
            EFContext context,
            IMapper mapper
        )
        {
            _context = context;
            _mapper = mapper;
        }

        public async Task<TodoActionViewModel> Handle(CreateTodoCommand request, CancellationToken cancelToken)
        {
            var todo = new TodoItem
            {
                Name = request.Name,
                Description = request.Description,
                Status = request.Status ?? false,
            };

            _context.Todos.Add(todo);
            await _context.SaveChangesAsync();

            var todos = await _context.Todos.ToListAsync();

            return new TodoActionViewModel()
            {
                Message = "Todo Created!",
                Todo = _mapper.Map<TodoItemViewModel>(todo),
                Todos = todos.Select(_mapper.Map<TodoItemViewModel>).ToList()
            };
        }
    }
}
