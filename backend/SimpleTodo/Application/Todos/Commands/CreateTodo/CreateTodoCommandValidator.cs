
using FluentValidation;

namespace SimpleTodo.Application.Todos.Commands.CreateTodo
{
    public class CreateTodoCommandValidator : AbstractValidator<CreateTodoCommand>
    {
        public CreateTodoCommandValidator()
        {
            RuleFor(ct => ct.Name)
                .NotEmpty().WithMessage("name field is required")
                .MaximumLength(100).WithMessage("Maximum character length exceeded. ({MaxLength} max.)");

            RuleFor(ct => ct.Description)
                .NotEmpty().WithMessage("description field is required")
                .MaximumLength(300).WithMessage("Maximum character length exceeded. ({MaxLength max.})");
        }
    }
}
