


namespace SimpleTodo.Application.Todos.DataTransfer
{
    public class UpdateTodoDto
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public bool Status { get; set; }
    }
}
