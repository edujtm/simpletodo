
using System.Collections.Generic;

namespace SimpleTodo.Application.Todos.DataTransfer
{
    public class TodoActionViewModel
    {
        public string Message { get; set; }

        public TodoItemViewModel Todo { get; set; }

        public IList<TodoItemViewModel> Todos { get; set; }
    }
}
