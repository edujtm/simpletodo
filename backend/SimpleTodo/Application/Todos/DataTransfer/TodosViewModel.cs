using System.Collections.Generic;

using SimpleTodo.Domain.Entities;

namespace SimpleTodo.Application.Todos.DataTransfer
{
    public class TodosViewModel
    {
        public IList<TodoItemViewModel> Todos { get; set; }
    }
}
