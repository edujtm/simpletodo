

namespace SimpleTodo.Application.Todos.DataTransfer
{
    public class TodoItemViewModel
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public bool Status { get; set; }
    }
}