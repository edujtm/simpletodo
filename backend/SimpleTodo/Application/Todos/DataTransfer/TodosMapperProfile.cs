
using AutoMapper;

using SimpleTodo.Domain.Entities;

namespace SimpleTodo.Application.Todos.DataTransfer
{
    public class TodosMapperProfile : Profile
    {
        public TodosMapperProfile()
        {
            CreateMap<TodoItem, TodoItemViewModel>();
        }
    }
}
