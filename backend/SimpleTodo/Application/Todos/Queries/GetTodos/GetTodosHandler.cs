using System.Threading;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

using AutoMapper;
using MediatR;
using AutoMapper.QueryableExtensions;

using SimpleTodo.Infra.Persistence;
using SimpleTodo.Application.Todos.DataTransfer;

namespace SimpleTodo.Application.Todos.Queries.GetTodos
{
    public class GetTodosHandler : IRequestHandler<GetTodosQuery, TodosViewModel>
    {
        private readonly EFContext _context;
        private readonly MapperConfiguration _config = new MapperConfiguration(cfg => 
        {
            cfg.AddProfile(new TodosMapperProfile());
        });

        public GetTodosHandler(EFContext context)
        {
            _context = context;
        }

        public async Task<TodosViewModel> Handle(GetTodosQuery request, CancellationToken cancelToken)
        {
            var todos = await _context.Todos
                .ProjectTo<TodoItemViewModel>(_config)
                .ToListAsync();

            return new TodosViewModel()
            {
                Todos = todos
            };
        }
    }
}
