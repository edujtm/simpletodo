using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

using MediatR;

using SimpleTodo.Application.Todos.DataTransfer;
using SimpleTodo.Application.Todos.Queries.GetTodos;
using SimpleTodo.Application.Todos.Commands.CreateTodo;
using SimpleTodo.Application.Todos.Commands.DeleteTodo;
using SimpleTodo.Application.Todos.Commands.UpdateTodo;


namespace SimpleTodo.Application.Todos
{
    [Route("api/todos/")]
    [ApiController]
    public class TodosController : ControllerBase
    {

        private readonly IMediator _mediator;

        public TodosController(IMediator mediator)
        {
            _mediator = mediator;
        }

        [HttpGet]
        public async Task<ActionResult<TodosViewModel>> GetTodos()
        {
            var todos = await _mediator.Send(new GetTodosQuery());

            return Ok(todos);
        }

        [HttpPost]
        public async Task<ActionResult<TodoActionViewModel>> CreateTodo(CreateTodoCommand command)
        {
            var todoResponse = await _mediator.Send(command);

            return Ok(todoResponse);
        }

        [HttpPut("{id:int}")]
        public async Task<ActionResult<TodoActionViewModel>> UpdateTodo(int id, UpdateTodoDto todo)
        {
            var todoResponse = await _mediator.Send(new UpdateTodoCommand()
            {
                Id = id,
                Name = todo.Name,
                Description = todo.Description,
                Status = todo.Status
            });

            return Ok(todoResponse);
        }

        [HttpDelete("{id:int}")]
        public async Task<ActionResult<TodoActionViewModel>> DeleteTodo(int id)
        {
            var todoResponse = await _mediator.Send(new DeleteTodoCommand { Id = id });

            return Ok(todoResponse);
        }
    }
}
