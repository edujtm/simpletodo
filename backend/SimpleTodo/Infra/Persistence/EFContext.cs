
using Microsoft.EntityFrameworkCore;

using SimpleTodo.Domain.Entities;

namespace SimpleTodo.Infra.Persistence
{
    public class EFContext : DbContext
    {
        public DbSet<TodoItem> Todos { get; set; }

        public EFContext(DbContextOptions<EFContext> options)
            : base(options) {}
    }
}