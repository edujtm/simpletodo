using System.Reflection;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Hosting;
using Microsoft.OpenApi.Models;

using MediatR;
using FluentValidation;
using AutoMapper;

using SimpleTodo.Infra.Persistence;
using SimpleTodo.Application.Common.Behaviors;
using SimpleTodo.Application.Common.Filters;

namespace SimpleTodo
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {

            services.AddControllers(options =>
            {
                options.Filters.Add<ApiExceptionFilterAttribute>();
            });

            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "SimpleTodo", Version = "v1" });
            });

            services.AddDbContext<EFContext>(options => 
            {
                options.UseSqlite(Configuration["ConnectionStrings:SqliteDB"]);
            });

            services.AddCors(opts => 
            {
                opts.AddDefaultPolicy(policy => 
                {

                    policy.WithOrigins(
                        Configuration["Origins:Http"],
                        Configuration["Origins:Https"]
                    )
                    .AllowAnyMethod()
                    .AllowAnyHeader();
                });
            });

            services.AddAutoMapper(Assembly.GetExecutingAssembly());
            services.AddValidatorsFromAssembly(Assembly.GetExecutingAssembly());

            services.AddMediatR(typeof(Startup));
            services.AddTransient(typeof(IPipelineBehavior<,>), typeof(ValidationBehavior<,>));
            services.AddTransient(typeof(IPipelineBehavior<,>), typeof(UnhandledExceptionBehavior<,>));
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, ILogger<Startup> logger)
        {

            logger.LogInformation("Http Origin {HttpOrigin}", Configuration["Origins:Http"]);
            logger.LogInformation("Https Origin {HttpsOrigin}", Configuration["Origins:Https"]);

            app.UseDeveloperExceptionPage();
            app.UseSwagger();
            app.UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json", "SimpleTodo v1"));

            // app.UseHttpsRedirection();

            app.UseRouting();
            app.UseCors();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapGet("/", (context) =>
                {
                    context.Response.Redirect("swagger/", permanent: false);
                    return Task.CompletedTask;
                });
                endpoints.MapControllers();
            });
        }
    }
}
