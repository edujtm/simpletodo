
using System.ComponentModel.DataAnnotations;

namespace SimpleTodo.Domain.Entities
{
    public class TodoItem
    {
        public int Id { get; set; }

        [Required]
        [MaxLength(100)]
        public string Name { get; set; }

        [Required]
        [MaxLength(1000)]
        public string Description { get; set; }

        public bool Status { get; set; }
    }
}